# Translations template for PROJECT.
# Copyright (C) 2022 ORGANIZATION
# This file is distributed under the same license as the PROJECT project.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2022.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2022-05-27 10:03+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.10.1\n"

#: templates/base.html:17
msgid "Welcome"
msgstr ""

#: templates/index.html:3
msgid "Welcome to BirdQuizz !"
msgstr ""

#: templates/menu.html:7
msgid "Home"
msgstr ""

#: templates/menu.html:8
msgid "Game"
msgstr ""

#: templates/menu.html:9
msgid "About"
msgstr ""

#: templates/menu.html:11
msgid "Logout"
msgstr ""

#: templates/menu.html:13
msgid "Login"
msgstr ""

#: templates/menu.html:15
msgid "Identify bird song"
msgstr ""

