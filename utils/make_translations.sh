#!/bin/sh
pybabel extract -F babel.cfg -o ./language/message.pot ./templates/**
pybabel update -i message.pot -d translations
pybabel compile -d translations