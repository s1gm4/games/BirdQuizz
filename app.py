from flask import Flask
from flask import render_template
from flask import session
from flask import request
from flask import redirect
from flask import url_for
from flask import g
from flask_migrate import Migrate

from flask_babel import Babel, gettext

from werkzeug.security import generate_password_hash, check_password_hash

from config import secret_key, database_uri
from model import db, User
import game as Game

app = Flask(__name__)
app.secret_key = secret_key

app.config['SQLALCHEMY_DATABASE_URI'] = database_uri
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

app.config['BABEL_DEFAULT_LOCALE'] = 'en'
app.config['BABEL_TRANSLATION_DIRECTORIES'] = "./language/translations"
babel = Babel(app)

db.init_app(app)

@babel.localeselector
def get_locale():
    if not session.get('lang') is None:
        return session['lang']
    # otherwise try to guess the language from the user accept
    # header the browser transmits.  We support fr/en in this
    # example.  The best match wins.
    return request.accept_languages.best_match(['fr', 'en'])

@babel.timezoneselector
def get_timezone():
    user = getattr(g, 'user', None)
    if user is not None:
        return user.timezone

app.jinja_env.globals['get_locale'] = get_locale

@app.route('/lang')
def get_lang():
    return render_template('lang.html')

@app.route('/lang/<locale>')
def set_lang(locale='en'):
    session['lang'] = locale
    return redirect('/')        

@app.route("/")
def home():
    if 'username' in session:
        return render_template("index.html", username=session["username"])
    else:
        return render_template("index.html")

@app.route("/signup", methods=["GET", "POST"])
def signup():
    if request.method == "POST":
        message = ""
        username = request.form['username']
        if username == "":
            message += gettext("Username empty. Try to find one.")
        email = request.form['email']
        if email == "":
            message += gettext("Email empty. Please give me one.")
        password = request.form['password']
        if password == "":
            message += gettext("You should not use an empty password")
        registered_user_by_username = User.query.filter_by(user_name=username).first()
        registered_user_by_email = User.query.filter_by(user_email=email).first()
        if registered_user_by_username is None and registered_user_by_email is None:
            password_hash = generate_password_hash(password)
            registered_user = User(user_name=username, user_email=email, user_password=password_hash)
            db.session.add(registered_user)
            db.session.commit()
        else:
            if not registered_user_by_email is None:
                message += gettext("Email already used by a user.")
            else:
                message += gettext("Username already used by a user.")
            return render_template("auth/signup.html", message=message)
        return redirect(url_for("login"))
    elif request.method == "GET":
        return render_template("auth/signup.html")

@app.route("/login", methods=["GET", "POST"])
def login():
    if request.method == "POST":
        username = request.form['username']
        password = request.form['password']
        user = User.query.filter_by(user_name=username).first()
        if user is None:
            return render_template("auth/login.html", message="No user with this username already registered")
        else:
            password_hash = user.user_password
            if check_password_hash(password_hash, password):
                session["username"] = username
                return redirect(url_for("home"))
            else:
               return render_template("auth/login.html", message="Password incorrect. Try again")
    if request.method == "GET":
        return render_template("auth/login.html")

@app.route("/logout")
def logout():
    # Remove username from the session if it's there
    session.pop("username", None)
    return redirect(url_for("home"))

# Game routes

@app.route("/game")
def game():
    return redirect(url_for('new_game'))

@app.route("/game/new")
def new_game():
    if not "username" in session:
        return redirect(url_for('login'))
    user = User.query.filter_by(user_name = session['username']).first()
    if user is None:
        return redirect(url_for('login'))
    else:
        level = str(user.user_level)
        question = Game.new_question(level)
        session["question"] = question
        return render_template("game/question.html", question=question)

@app.route("/game/answer", methods=["POST", "GET"])
def game_answer():
    if request.method == "POST":
        answer = request.form["answer"]
        if answer == session["question"]["species"]:
            message = "You are correct !" 
        else:
            message = "You are not correct !"
        return render_template("game/answer.html", message=message, question=session["question"])
    elif request.method == "GET":
        return render_template("game/new.html")
    

@app.route("/about")
def about():
    return render_template("about/index.html")

migrate = Migrate(app, db)