# BirdQuizz

Identify Bird Song !


# Installation

```bash
git clone https://forge.chapril.org/UncleSamulus/BirdQuizz.git
cd BirdQuizz
python3 -m venv
. venv/bin/activate
pip install -r requirements.txt
```

## Database configuration

```bash
sudo mysql
```

```sql
CREATE DATABASE birdquizz;
GRANT ALL ON birdquizz.* TO birdquizz@localhost IDENTIFIED BY 'secret';
``` 

## Configuration

```bash
cp config.py.example config.py
```

## Database migration

```bash
flask db migrate -m "Migration message."
flask db upgrade # Perform migration (after script verification in ./migrations/versions/)
```