import random
import os
from glob import glob
import json

with open("./data/level_species_cleaned.json", "r") as f:
    LEVEL_SPECIES_LIST = json.load(f)

format_name = lambda folder_name : folder_name.replace('_', ' ')
to_folder_name = lambda species_name : species_name.replace(' ', '_').replace('\'', '').lower()

def get_proposals(question_species_name, available_species, n):
    proposals = [question_species_name]
    for i in range(n):
        proposition = random.choice(available_species)
        while proposition == question_species_name:
            proposition = random.choice(available_species)
        proposals.append(proposition)
    random.shuffle(proposals)
    return proposals

def new_question(level):
    available_species = LEVEL_SPECIES_LIST[level]
    question_species_name = random.choice(available_species)
    question_species_folder = to_folder_name(question_species_name)
    question = {}
    audio_paths = list(map(os.path.basename, glob(f"static/data/src_audio/{question_species_folder}/*.mp3")))
    audio_path = random.choice(audio_paths)
    question["species"] = question_species_name
    question["species_folder"] = question_species_folder
    question["audio_path"] = audio_path
    question["proposals"] = get_proposals(question_species_name, available_species, 5)
    return question
