from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm import declarative_base, relationship

db = SQLAlchemy()

class User(db.Model):
    __tablename__ = 'user'

    user_id = db.Column(db.Integer, primary_key=True)
    user_name = db.Column(db.String(64), unique=True)
    user_email = db.Column(db.String(64), unique=True, index=True)
    user_password = db.Column(db.String(128))
    user_level = db.Column(db.Integer, default=1)
    user_score = db.Column(db.Integer, default=0)